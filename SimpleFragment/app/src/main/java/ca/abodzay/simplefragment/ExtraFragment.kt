package ca.abodzay.simplefragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment

class ExtraFragment : Fragment(), View.OnClickListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.extra_fragment, container, false)
        val b = v.findViewById<Button>(R.id.button2)
        b.setOnClickListener(this)
        return v
    }

    override fun onClick(view: View){
        if(view.id == R.id.button2 )
        {
            val a = activity as MainActivity
            a?.updateFragment1()
        }
    }
}